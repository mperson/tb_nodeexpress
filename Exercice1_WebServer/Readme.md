# Exercice 1 - Web Server
## _Premier exercice de mise en place d'un serveur NodeJS basique_
 

## Enoncé

   - J'ai besoin d'avoir une page Home en HTML qui contient un bref descriptif de votre parcours
   - J'ai besoin d'une page about qui contiendra 1 photo (de vous ou autre) + informations "personnelles" (nom, prenom) + votre slogan
   - Un formulaire de contact permettant de vous envoyer un pettit message 

## Contraintes :

> Utiliser des conditions pour séparer les verbs GET et POST
> Utiliser pour la navigation le pattern suivant
>  /Home ou / ==> page Home
> /About ==> page about
> /Contact ==> formulaire
> /Merci ==> pour le retour du formulaire

 

## Liens utiles

Dillinger uses a number of open source projects to work properly:

- [Redirection](https://sebhastian.com/node-js-redirect/) - Comment effectuer une redirection en NodeJS
- [Image](https://www.geeksforgeeks.org/how-to-fetch-images-from-node-js-server/) - Comment afficher une image avec nodejs (avec ou sans expressJs)
- [NodeMailer](https://nodemailer.com/about/) - Envoyer un mail avec Nodejs.
- [smtp4dev](https://github.com/rnwood/smtp4dev) - Fake local smtp server