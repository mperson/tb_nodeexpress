const http = require('http'); //importe le module http qui va me permettre d'effectuer la gestion des request et response
const url = require('url'); //module permettant le parsing de l'url
const {parse} = require("querystring"); //Utilisée pour parser mon POST 
const server = http.createServer(function(req,res)
{
     console.log(req.method);
     //REQUEST
     //console.log(req.headers);
     let contenu ="";
     console.log(req.url);
      
    if(req.method=="GET")
    {
        let urlDecode = url.parse(req.url,true);
        console.log(urlDecode.query);
        if(urlDecode.query.page==="info")
        {
             contenu="<h1>Ce site est développé par Mike</h1>";
        }
        if(urlDecode.query.page==="Home" || req.url=="/")
        {
              let Nom = urlDecode.query.nom;
             contenu=`<h1>Bonjour ${Nom}<br />(traduction de : Hello ${Nom})</h1>`;
        }
        if(urlDecode.query.page==="Contact")
        {
             contenu=`<form method='POST' action='/'>
                         Nom : <input type="text" name="Nom" /> <br />
                         Age : <input type="number" name="Age" /><br /> 
                         <button>Send</button>
                      </form>`;
        }
      
      
         //1- S'occuper du header
         res.writeHead(200, {"Content-Type":"text/html"});
 
         //2- Envoyer mon contenu
         res.end(`<DOCTYPE!><html><body>${contenu}</body></html>`);
    }
    if(req.method=="POST")
    {
        
        let infoPost='';
        req.on("data", form => {infoPost+= form.toString(); console.log("Récupération du contenu du formulaire"); });
        req.on("end", ()=>
        {
              let monForm =parse(infoPost); 
              res.writeHead(204);

              res.end();

        });
    }

   
});

server.listen(8002, ()=>{ console.log("Le serveur vous attend sur http://localhost:8002/")});