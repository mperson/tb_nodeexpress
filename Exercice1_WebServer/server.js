const http = require('http'); //importe le module http qui va me permettre d'effectuer la gestion des request et response
const url = require('url'); //module permettant le parsing de l'url
const {parse} = require("querystring"); //Utilisée pour parser mon POST 
const StaticFile = require("./Modules/static");
const HtmlCompose= require("./Modules/compose");
const mail = require("./Modules/mail");
const server = http.createServer(function(req,res)
{
     
    if(req.url.startsWith("/Assets"))
     {
        StaticFile.get(`.${req.url}`, res);
     }
     else
     {
        let isFound=false;
        let body="";
        if(req.url==="/" || req.url==="/Home")
        {
            isFound=true;
            body = HtmlCompose.header();
            body+= HtmlCompose.content("<h1>Mike Person</h1><br/><i>Co-Fondateur Cognitic SPRL, Developer,Consultant and Instructor</i>");
            body+= HtmlCompose.footer();           
        } 
        if(req.url==="/About")
        {
            isFound=true;
            body = HtmlCompose.header();
            body+= HtmlCompose.content("<img src='/Assets/images/images.jpg'><br>C'est moi qui fait ça ? <br/> <pre>Sauve un arbre, mange un castor</pre>");
            body+= HtmlCompose.footer();           
        } 
        
        if(req.url==="/Contact")
        {
            if(req.method==="GET")
            {
                isFound=true;
                contenu=`<br><form method='POST' action='/Contact'>
                         Nom : <input type="text" name="Nom" /> <br />
                         Message : <textarea name="message" ></textarea><br /> 
                         <button>Send</button>
                      </form>`;
                body = HtmlCompose.header();
                body+= HtmlCompose.content(contenu);
                body+= HtmlCompose.footer();           
            }
            else
            {

                let infoPost='';
                req.on("data", form => {infoPost+= form.toString(); console.log("Récupération du contenu du formulaire"); });
                req.on("end", ()=>
                {
                    let monForm =parse(infoPost); 
                    let message=`Message : ${monForm.message} <hr/> Envoyé par : ${monForm.Nom}`;
                    mail.send(message).then((e)=>{
                        console.log(e);
                        console.log("Mail envoyé");
                       //redirection
                          res.writeHead(302, {
                          location: "/Merci",
                        });
                        res.end();});

                });
                 
            }
        } 
        
        
        if(req.url=="/Merci")
        {
            isFound=true;
            body = HtmlCompose.header();
            body+= HtmlCompose.content("<br><h1>Merci. Votre message a été envoyé</h1>");
            body+= HtmlCompose.footer();           
        } 

        if(!isFound)
        {
            StaticFile.get(`./Assets/404.html`, res);
        }
        else
        {         
            res.writeHead(200, {"Content-Type":"text/html"}); 
            res.end(body); 
        }
     }
    
     

   
});

server.listen(8002, ()=>{ console.log("Le serveur vous attend sur http://localhost:8002/")});