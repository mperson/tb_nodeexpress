// Requiring modules
const http = require("http");
const fs = require("fs");
const path = require("path");
const url = require("url");
const { exec } = require("child_process");

var StaticFile = 
{

    get: function(filePath, res)
    { 
        fs.open(filePath, 'r', (err, fd) => {
            if (err) {
              if (err.code === 'ENOENT') {
                res.writeHead(404, {
                    "Content-Type": "text/plain" });
                res.end("404 Not Found");
                return;
              }
          
              throw err;
            }

            let re = /(?:\.([^.]+))?$/;
            let extension = re.exec(filePath)[1];
            console.log(filePath);
            console.log(re.exec(filePath))
            let contentType="text/plain";
            switch(extension)
            {
                case 'css': contentType="text/css";break;
                case 'js':contentType="text/javascript";break;
                case 'jpg':contentType="image/jpeg";break;
                case 'html':contentType="text/html";break;
                
            };
                    
            // Setting the headers
            res.writeHead(200, {
                "Content-Type": contentType });

            try {
                fs.readFile(filePath,
                    function (err, content) { 
                        res.end(content);
                    });
            } finally {
              fs.close(fd, (err) => {
                if (err) throw err;
              });
            }
          });
    }


}
module.exports=StaticFile;