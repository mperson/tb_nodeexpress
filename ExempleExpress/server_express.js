const express = require("express");
const app = express();
 

app.all('/', (req,res, next)=>
 {
     console.log("Url demandée:" + req.url);
     console.log("Méthod:" + req.method);
     next();
 });



app.get('/', (req,res)=>{
 
    res.writeHead(200, {"Content-Type":"text/html"}); 
    res.end("Hello");
 });
 app.get('/Hello/:prenom/Age/:age', (req,res)=>
 {

    console.log(req.params);
   /* res.writeHead(200, {"Content-Type":"text/html"}); 
    res.end(`Hello   ${req.params.prenom} , vous avez ${req.params.age} an(s)`);*/
    res.status(200).send(`Hello   ${req.params.prenom} , vous avez ${req.params.age} an(s)`);
 });

 //En cas d'échec absolu concernant la route
  app.all('*', (req,res)=>
 {
     res.status(404).send("404");
 });
 

app.listen(8002,console.log("Votre serveur express vous attend sur http://127.0.0.1:8002"))